package com.brouzex;

public class AdditionalPayment {
	private int month;
	private double amount;
	
	public AdditionalPayment(int month, double amount) {
		super();
		this.month = month;
		this.amount = amount;
	}

	public int getMonth() {
		return this.month;
	}

	public double getAmount() {
		return this.amount;
	}
	
	
}
