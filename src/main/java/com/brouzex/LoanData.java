package com.brouzex;

import java.time.LocalDate;

public class LoanData {
	private double loanAmount;
	private double interestRate;
	private int installments;
	private LocalDate startDate;
	
	public LoanData(double loanAmount, double interestRate, int years){
		this.startDate = LocalDate.now().plusMonths(1);
		this.loanAmount = loanAmount;
		this.interestRate = interestRate;
		this.installments = years*12;
	}

	public double getLoanAmount() {
		return this.loanAmount;
	}

	public double getInterestRate() {
		return this.interestRate;
	}

	public int getInstallments() {
		return this.installments;
	}
	
	public LocalDate getStartDate() {
		return this.startDate;
	}

	@Override
	public String toString() {
		return "Loan of "+this.loanAmount+" for "+this.installments+" months with "+this.interestRate+"% interest rate";
	}
}
