package com.brouzex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class LoanApplication{

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(LoanApplication.class, args);
		
		ConsoleRunner runner = ctx.getBean(ConsoleRunner.class);
		runner.start();
	}
}
