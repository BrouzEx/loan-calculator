package com.brouzex;

public class StringUtil {
	
	public static String fillSpaces(String text, int expectedLength){
		return fillSpaces(text, expectedLength, ' ');
	}
	
	public static String fillSpaces(String text, int expectedLength, char fillChar){
		StringBuilder sb = new StringBuilder(text);
		for(int i=0; i<expectedLength-text.length(); ++i){
			sb.insert(0, fillChar);
		}
		return sb.toString();
	}
}
