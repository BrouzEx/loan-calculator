package com.brouzex;

import java.time.LocalDate;

public class AnnuityData {
	private int number;
	private LocalDate date;
	private double principal;
	private double interest;
	private double remainingDebt;
	private double interestDue;
	
	public AnnuityData(int number, LocalDate date, double principal, double interest, double remainingDebt,
			double interestDue) {
		super();
		this.number = number;
		this.date = date;
		this.principal = principal;
		this.interest = interest;
		this.remainingDebt = remainingDebt;
		this.interestDue = interestDue;
	}

	public int getNumber() {
		return this.number;
	}

	public LocalDate getDate() {
		return this.date;
	}

	public double getPrincipal() {
		return this.principal;
	}

	public double getInterest() {
		return this.interest;
	}

	public double getRemainingDebt() {
		return this.remainingDebt;
	}

	public double getInterestDue() {
		return this.interestDue;
	}
	
	
}
