package com.brouzex;

import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class AnnuityPrinter {
	private DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	private DecimalFormat nf = new DecimalFormat("#0.00");
	
	private static int numLength=3, dateLength=15, principalLength=10, interestLength=10, remainingLength=15, dueLength=15;
	private static int totalLength;
	static{
		totalLength = numLength+dateLength+principalLength+interestLength+remainingLength+dueLength;
	}
	
	public AnnuityPrinter(){
		
	}
	
	public void print(List<AnnuityData> annuities){
		print(annuities, null);
	}
	
	public void print(List<AnnuityData> annuities, List<AdditionalPayment> additionalPayments){
		System.out.println(
				StringUtil.fillSpaces("Num", numLength)+
				StringUtil.fillSpaces("Date", dateLength)+
				StringUtil.fillSpaces("Principal", principalLength)+
				StringUtil.fillSpaces("Interest", interestLength)+
				StringUtil.fillSpaces("Remaining debt", remainingLength)+
				StringUtil.fillSpaces("Interest due", dueLength)
				);
		System.out.println(StringUtil.fillSpaces("", totalLength, '-'));
		
		for(AnnuityData annuity : annuities){
			if(additionalPayments!=null){
				for(AdditionalPayment additionalPayment : additionalPayments){
					if(additionalPayment.getMonth()==annuity.getNumber()){
						System.out.println(StringUtil.fillSpaces("", totalLength, '-'));
						System.out.println("Additionally payed: "+this.nf.format(additionalPayment.getAmount()));
						System.out.println(StringUtil.fillSpaces("", totalLength, '-'));
						break;
					}
				}
			}
			
			System.out.println(
					StringUtil.fillSpaces(annuity.getNumber()+"", numLength)+
					StringUtil.fillSpaces(annuity.getDate().format(this.df), dateLength)+
					StringUtil.fillSpaces(this.nf.format(annuity.getPrincipal()), principalLength)+
					StringUtil.fillSpaces(this.nf.format(annuity.getInterest())+"", interestLength)+
					StringUtil.fillSpaces(this.nf.format(annuity.getRemainingDebt())+"", remainingLength)+
					StringUtil.fillSpaces(this.nf.format(annuity.getInterestDue())+"", dueLength)
					);
		}
	}
}
