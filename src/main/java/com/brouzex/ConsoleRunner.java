package com.brouzex;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConsoleRunner {
	
	@Autowired
	private AnnuityPrinter annuityPrinter;

	public void start() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Loan amount: ");
		double loanAmount = scanner.nextDouble();
		System.out.println("Interest rate: ");
		double interestRate = Double.parseDouble(scanner.next().replace(",", "."));
		System.out.println("Number of years: ");
		int years = scanner.nextInt();
		
		LoanData loanData = new LoanData(loanAmount, interestRate, years);
		AnnuityCalculator annutityCalculator = new AnnuityCalculator(loanData);
		System.out.println(loanData);
		
		System.out.println("Annuity amount: "+Math.round(annutityCalculator.getAnnuityAmount()*100)/100d);
		
		System.out.println("Annuities");
		this.annuityPrinter.print(annutityCalculator.getAnnuities());
		System.out.println();
		
		List<AdditionalPayment> additionalPayments = new ArrayList<>();
		System.out.println("Enter additional payments in form \"month,amount\". When done enter 0");
		scanner.nextLine();
		while(true){
			String input = scanner.nextLine();
			if("0".equals(input)) break;
			
			if(!input.contains(",")){
				System.out.println("Invalid entry, missing comma");
				continue;
			}
			
			String[] inputParams = input.split(",");
			int month;
			double amount;
			try{
				month = Integer.parseInt(inputParams[0]);
			} catch(NumberFormatException e){
				System.out.println("Invalid entry, month is not a number");
				continue;
			}
			try{
				amount = Double.parseDouble(inputParams[1]);
			} catch(NumberFormatException | ArrayIndexOutOfBoundsException e){
				System.out.println("Invalid entry, amount is not a number");
				continue;
			}
			
			System.out.println("Accepted entry for month="+month+" iand amount="+amount);
			additionalPayments.add(new AdditionalPayment(month, amount));
		}
		
		if(additionalPayments.size()>0){
			System.out.println("\nLoan calculation with additional payments");
			AnnuityCalculator annuityCalculator2 = new AnnuityCalculator(loanData, additionalPayments);
			this.annuityPrinter.print(annuityCalculator2.getAnnuities(), additionalPayments);
		}
		
	}

}
