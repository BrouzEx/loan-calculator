package com.brouzex;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AnnuityCalculator {
	private LoanData loanData;
	private static int annuityPeriodicity = 12; //12 months
	private double annuityAmount;
	private List<AdditionalPayment> additionalPayments;
	
	public AnnuityCalculator(LoanData loanData){
		this(loanData, null);
	}
	
	public AnnuityCalculator(LoanData loanData, List<AdditionalPayment> additionalPayments){
		this.loanData = loanData;
		this.annuityAmount = -1*Formulas.pmt(loanData.getInterestRate()/100/annuityPeriodicity, loanData.getInstallments(), loanData.getLoanAmount());
		this.additionalPayments = additionalPayments;
	}
	
	public double getAnnuityAmount(){
		return this.annuityAmount;
	}
	
	public List<AnnuityData> getAnnuities(){
		List<AnnuityData> annuities = new ArrayList<>();
		int annuityCount = 1;
		double remainingDebt = this.loanData.getLoanAmount();
		double interestDue = 0;
		LocalDate date = this.loanData.getStartDate();
		
		do {
			if(this.additionalPayments!=null){
				for(AdditionalPayment additionalPayment : this.additionalPayments){
					if(additionalPayment.getMonth()==annuityCount){
						remainingDebt -= additionalPayment.getAmount();
						break;
					}
				}
			}
			double principal = -1*Formulas.ppmt(this.loanData.getInterestRate()/100/annuityPeriodicity, annuityCount, this.loanData.getInstallments(), this.loanData.getLoanAmount());
			double interest = this.annuityAmount-principal;
			remainingDebt -= principal;
			interestDue += interest;
			date = date.plusMonths(1);
			annuities.add(new AnnuityData(annuityCount, date, principal, interest, remainingDebt, interestDue));
			annuityCount++;
		} while(annuityCount<=this.loanData.getInstallments() && remainingDebt>0);
		
		return annuities;
	}
}
